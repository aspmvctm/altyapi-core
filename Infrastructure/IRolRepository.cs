﻿using System;
using Altyapi.Data.Model;

namespace Altyapi.Core.Infrastructure
{
    public interface IRolRepository : IRepository<Rol>, IDisposable
    {
    }
}
