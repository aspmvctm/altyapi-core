﻿using System;
using Altyapi.Data.Model;

namespace Altyapi.Core.Infrastructure
{
    public interface IKullaniciRepository :  IRepository<Kullanici>, IDisposable
    {
    }
}
