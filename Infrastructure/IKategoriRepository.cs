﻿using System;
using Altyapi.Data.Model;

namespace Altyapi.Core.Infrastructure
{
    public interface IKategoriRepository :IRepository<Kategori>, IDisposable
    {
    }
}
