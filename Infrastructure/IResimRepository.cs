﻿using System;
using Altyapi.Data.Model;

namespace Altyapi.Core.Infrastructure
{
    public interface IResimRepository :IRepository<Resim>, IDisposable
    {
    }
}
