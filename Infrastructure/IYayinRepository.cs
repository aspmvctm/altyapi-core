﻿using System;
using Altyapi.Data.Model;

namespace Altyapi.Core.Infrastructure
{
    public interface IYayinRepository : IRepository<Yayin>, IDisposable
    {
    }
}
