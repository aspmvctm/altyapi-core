﻿using System;
using Altyapi.Data.Model;

namespace Altyapi.Core.Infrastructure
{
    public interface IEtiketRepository : IRepository<Etiket>, IDisposable
    {
    }
}
