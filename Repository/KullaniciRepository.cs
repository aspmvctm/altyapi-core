﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using Altyapi.Core.Infrastructure;
using Altyapi.Data.DataContext;
using Altyapi.Data.Model;

namespace Altyapi.Core.Repository
{
    public class KullaniciRepository :IKullaniciRepository
    {
        private readonly AltyapiContext _context = new AltyapiContext();

        public int Count()
        {
            return _context.Kullanici.Count();
        }

        public void Delete(Kullanici obj)
        {
            _context.Kullanici.Remove(obj);
        }

        public IQueryable<Kullanici> GetMany(Expression<Func<Kullanici, bool>> expression)
        {
            return _context.Kullanici.Where(expression);
        }

        public Kullanici Get(Expression<Func<Kullanici, bool>> expression)
        {
            return _context.Kullanici.FirstOrDefault(expression);
        }

        public IEnumerable<Kullanici> GetAll()
        {
            return _context.Kullanici.Select(x => x);
        }

        public Kullanici GetById(int id)
        {
            return _context.Kullanici.FirstOrDefault(x => x.ID == id);
        }

        public void Insert(Kullanici obj)
        {
            _context.Kullanici.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Kullanici obj)
        {
            _context.Kullanici.AddOrUpdate(obj);
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
