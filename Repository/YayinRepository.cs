﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using Altyapi.Core.Infrastructure;
using Altyapi.Data.DataContext;
using Altyapi.Data.Model;

namespace Altyapi.Core.Repository
{
    public class YayinRepository : IYayinRepository
    {
        private readonly AltyapiContext _context = new AltyapiContext();

        public int Count()
        {
           return _context.Yayin.Count();
        }

        public void Delete(Yayin obj)
        {
            _context.Yayin.Remove(obj);
        }

        public IQueryable<Yayin> GetMany(Expression<Func<Yayin, bool>> expression)
        {
            return _context.Yayin.Where(expression);
        }

        public Yayin Get(Expression<Func<Yayin, bool>> expression)
        {
            return _context.Yayin.FirstOrDefault(expression);
        }

        public IEnumerable<Yayin> GetAll()
        {
            return _context.Yayin.Select(x => x);
        }

        public Yayin GetById(int id)
        {
            return _context.Yayin.FirstOrDefault(x => x.ID == id);
        }

        public void Insert(Yayin obj)
        {
            _context.Yayin.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Yayin obj)
        {
            _context.Yayin.AddOrUpdate(obj);
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
