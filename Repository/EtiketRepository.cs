﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using Altyapi.Core.Infrastructure;
using Altyapi.Data.DataContext;
using Altyapi.Data.Model;

namespace Altyapi.Core.Repository
{
    public class EtiketRepository : IEtiketRepository
    {
        private readonly AltyapiContext _context = new AltyapiContext();

        public int Count()
        {
            return _context.Etiket.Count();
        }

        public void Delete(Etiket obj)
        {
            _context.Etiket.Remove(obj);
        }

        public IQueryable<Etiket> GetMany(Expression<Func<Etiket, bool>> expression)
        {
            return _context.Etiket.Where(expression);
        }

        public Etiket Get(Expression<Func<Etiket, bool>> expression)
        {
            return _context.Etiket.FirstOrDefault(expression);
        }

        public IEnumerable<Etiket> GetAll()
        {
            return _context.Etiket.Select(x => x);
        }

        public Etiket GetById(int id)
        {
            return _context.Etiket.FirstOrDefault(x => x.ID == id);
        }

        public void Insert(Etiket obj)
        {
            _context.Etiket.Add(obj);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(Etiket obj)
        {
            _context.Etiket.AddOrUpdate(obj);
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
